cp ~/dotfiles/.emacs ~/.emacs

mkdir ~/.xmonad
cp ~/dotfiles/xmonad.hs ~/.xmonad/
cp ~/dotfiles/.xmobarrc ~/.xmobarrc
